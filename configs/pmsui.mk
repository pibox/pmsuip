# ---------------------------------------------------------------
# Build PiBox Media Server UI
# ---------------------------------------------------------------

root-verify:
	@if [ $(UID) -ne 0 ]; then \
		$(MSG11) "You must be root." $(EMSG); \
		exit 1; \
	fi

opkg-verify:
	@if [ "$(OPKG_DIR)" = "" ] || [ ! -f $(OPKG_DIR)/opkg-build ]; then \
		$(MSG11) "Can't find opkg-build.  Try setting OPKG= to the directory it lives in." $(EMSG); \
		exit 1; \
	fi

# Retrieve package
.$(PMSUI_T)-get $(PMSUI_T)-get: 
	@touch .$(subst .,,$@)

.$(PMSUI_T)-get-patch $(PMSUI_T)-get-patch: .$(PMSUI_T)-get
	@touch .$(subst .,,$@)

# Unpack packages
.$(PMSUI_T)-unpack $(PMSUI_T)-unpack: .$(PMSUI_T)-get-patch
	@touch .$(subst .,,$@)

# Apply patches
.$(PMSUI_T)-patch $(PMSUI_T)-patch: .$(PMSUI_T)-unpack
	@touch .$(subst .,,$@)

.$(PMSUI_T)-init $(PMSUI_T)-init: .$(PMSUI_T)-patch
	@touch .$(subst .,,$@)

.$(PMSUI_T)-config $(PMSUI_T)-config: 

# Build the package
$(PMSUI_T): .$(PMSUI_T)

.$(PMSUI_T): .$(PMSUI_T)-init 
	@make --no-print-directory $(PMSUI_T)-config
	@touch .$(subst .,,$@)

$(PMSUI_T)-files:

# Package it as an opkg 
opkg pkg $(PMSUI_T)-pkg: .$(PMSUI_T)
	@make --no-print-directory root-verify opkg-verify
	@$(MSG) "================================================================"
	@$(MSG3) "Building opkg" $(EMSG)
	@$(MSG) "================================================================"
	@mkdir -p $(PKGDIR)/opkg/pmsui/CONTROL
	@cp -ar $(DIR_PMSUI)/config/* $(PKGDIR)/opkg/pmsui/
	@cp $(DIR_PMSUI)/opkg/control $(PKGDIR)/opkg/pmsui/CONTROL/control
	@cp $(DIR_PMSUI)/opkg/postinst $(PKGDIR)/opkg/pmsui/CONTROL/postinst
	@cp $(DIR_PMSUI)/opkg/postrm $(PKGDIR)/opkg/pmsui/CONTROL/postrm
	@cp $(DIR_PMSUI)/opkg/debian-binary $(PKGDIR)/opkg/pmsui/CONTROL/debian-binary
	@chmod +x $(PKGDIR)/opkg/pmsui/CONTROL/postinst
	@chmod +x $(PKGDIR)/opkg/pmsui/CONTROL/postrm
	@chown -R root.root $(PKGDIR)/opkg/pmsui/
	@cd $(PKGDIR)/opkg/ && $(OPKG_DIR)/opkg-build -O pmsui
	@cp $(PKGDIR)/opkg/*.opk $(PKGDIR)/
	@rm -rf $(PKGDIR)/opkg

# Clean the packaging
pkg-clean $(PMSUI_T)-pkg-clean:
	@if [ "$(PKGDIR)" != "" ] && [ -d "$(PKGDIR)" ]; then rm -rf $(PKGDIR); fi

# Clean out a cross compiler build but not the CT-NG package build.
$(PMSUI_T)-clean: $(PMSUI_T)-pkg-clean
	@if [ "$(PMSUI_SRCDIR)" != "" ] && [ -d "$(PMSUI_SRCDIR)" ]; then rm -rf $(PMSUI_SRCDIR); fi
	@rm -f .$(PMSUI_T) .$(PMSUI_T)-get

# Clean out everything associated with PMSUI
$(PMSUI_T)-clobber: 
	@rm -rf $(BLDDIR) 
	@rm -f .$(PMSUI_T)-config .$(PMSUI_T)-init .$(PMSUI_T)-patch \
		.$(PMSUI_T)-unpack .$(PMSUI_T)-get .$(PMSUI_T)-get-patch
	@rm -f .$(PMSUI_T) 

